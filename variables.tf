variable "LINODE_TOKEN" {
  type = string
}

variable "instance" {
  type = string
}

variable "revision" {
  type = string
}

variable "discord" {
  type = string
}

variable "gitlab_user" {
  type = string
}

variable "gitlab_pass" {
  type = string
}
