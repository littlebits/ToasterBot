locals {
  kubeconfig = yamldecode(base64decode(linode_lke_cluster.toasterCluster.kubeconfig))
}

provider "linode" {
  token = var.LINODE_TOKEN
}

resource "linode_lke_cluster" "toasterCluster" {
  label       = "toasterCluster"
  k8s_version = "1.24"
  region      = "us-southeast"
  tags        = ["toaster", "discord"]

  pool {
    type  = "g6-dedicated-2"
    count = 1
  }
}

provider "kubernetes" {
  host                   = local.kubeconfig.clusters[0].cluster.server
  token                  = local.kubeconfig.users[0].user.token
  cluster_ca_certificate = base64decode(local.kubeconfig.clusters[0].cluster.certificate-authority-data)
}

resource "kubernetes_secret" "toasterBotToken" {
  metadata {
    name = "toasterbot-${var.instance}"
    labels = {
      app      = "toasterbot"
      instance = "${var.instance}"
    }
  }
  data = {
    token = "${var.discord}"
  }
}

resource "kubernetes_secret" "regcred" {
  metadata {
    name = "regcred"
    labels = {
      app = "toasterbot"
    }
  }
  type = "kubernetes.io/dockerconfigjson"
  data = {
    ".dockerconfigjson" = "{ \"auths\" : { \"registry.gitlab.com\" : { \"username\" : \"${var.gitlab_user}\", \"password\" : \"${var.gitlab_pass}\" } } }"
  }
}

resource "kubernetes_deployment" "toasterBot" {
  metadata {
    name = "toasterbot-${var.instance}"
    labels = {
      app      = "toasterbot"
      instance = "${var.instance}"
    }
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        app      = "toasterbot"
        instance = "${var.instance}"
      }
    }
    template {
      metadata {
        labels = {
          app      = "toasterbot"
          instance = "${var.instance}"
        }
      }
      spec {
        image_pull_secrets {
          name = "regcred"
        }
        container {
          image             = "registry.gitlab.com/littlebits/toasterbot:${var.revision}"
          image_pull_policy = "Always"
          name              = "toasterbot"
          env {
            name = "TOKEN"
            value_from {
              secret_key_ref {
                name = "toasterbot-${var.instance}"
                key  = "token"
              }
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_namespace" "monitoring" {
  metadata {
    name = "monitoring"
  }
}

provider "helm" {
  kubernetes {
    host                   = local.kubeconfig.clusters[0].cluster.server
    token                  = local.kubeconfig.users[0].user.token
    cluster_ca_certificate = base64decode(local.kubeconfig.clusters[0].cluster.certificate-authority-data)

  }
}

resource "helm_release" "metrics-server" {
  name       = "metrics-server"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "metrics-server"
  namespace  = "monitoring"

  set {
    name  = "startupProbe.initialDelaySeconds"
    value = "120s"
  }

  set {
    name  = "apiService.create"
    value = "true"
  }

  set {
    name  = "extraArgs[0]"
    value = "--kubelet-preferred-address-types=InternalIP"
  }

  set {
    name  = "extraArgs[1]"
    value = "--kubelet-insecure-tls"
  }

  depends_on = [
    kubernetes_namespace.monitoring
  ]
}

