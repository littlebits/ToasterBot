terraform {
  cloud {
    organization = "frenchtoasters"
    workspaces {
      name = "ToasterBot"
    }
  }
  required_providers {
    linode = {
      source  = "linode/linode"
      version = "1.29.4"
    }
  }
}
