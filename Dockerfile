FROM node:16.14

ENV USER=toaster

# install python and make
RUN apt-get update && \
	apt-get install -y python3 build-essential git && \
	apt-get purge -y --auto-remove

# create evobot user
RUN groupadd -r ${USER} && \
	useradd --create-home --home /home/toaster -r -g ${USER} ${USER}

# set up volume and user
USER ${USER}
WORKDIR /home/toaster

RUN git clone https://github.com/frenchtoasters/evobot.git && cd evobot && \
	git checkout playdlswitch

RUN cd evobot && chown ${USER}:${USER} package*.json
RUN cd evobot && npm install
VOLUME [ "/home/toaster/evobot" ]
RUN cd evobot && chown ${USER}:${USER}  .

WORKDIR /home/toaster/evobot

ENTRYPOINT [ "npm", "run", "prod" ]
